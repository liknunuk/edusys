var today = new Date();
var dina = today.getDate();
var sasi = today.getMonth();
var taun = today.getFullYear();
let wulan = wulanapa(sasi);

var kelas = '', absen = '';

var hariini = "Banjarngara, " + dina + ' ' + wulan + ' ' + taun;
$("#hariini").html(hariini);
// dataKelas
$.getJSON(pusdat + `dataKelas` , function(kelas){
    $('#kelas').html('');
    $.each( kelas , function(i,data){
        $('#kelas').append(`
            <button class='btn btn-primary btn-kelas'>${data.kelas}</button>
        `);
    })
})

$('#kelas').on( 'click' , 'button' , function(){
    kelas = $(this).text();
})

for( let i = 0 ; i < 40 ; i++ ){
    let abs = i+1;
    $('#absen').append(`
        <button class='btn btn-primary btn-absen'>${abs}</button>
    `);
}

$('#absen').on( 'click' , 'button' , function(){
    absen = $(this).text();

    let info = "Kelas " + kelas + " No. Absen : " + absen;
    $.getJSON( pusdat + `siswaByKlAbs/${kelas}/${absen}` , function (siswa){
        $('.sisinfo').html('');
        $('#sisnis').html(siswa.nis);
        $('#sisnama').html(siswa.nama);
        $('#siskls').html(kelas);
        $('#sisabs').html(absen);

        localStorage.setItem('swnis',siswa.nis);
    })
})

$(".li-sholat").click( function(){
    let sholubar = $(this).children('div.form-check').children('input');;
    if( sholubar.prop('checked') == true){
        sholubar.prop('checked',false);
    }else{
        sholubar.prop('checked',true);
    }
})

$('#btnSholat').click( function(){
    $('#sholatq').modal('toggle');
    let dzu,ash,mgr,ish,sbh,data1;
    dzu = isceked("#cbDzuhur");
    ash = isceked("#cbAshar");
    mgr = isceked("#cbMaghrib");
    ish = isceked("#cbIsha");
    sbh = isceked("#cbSubuh");
    data1 = `${sbh}-${dzu}-${ash}-${mgr}-${ish}`;
    localStorage.setItem('sholate' , data1);
    // data = {'dzuhur':dzu , 'ashar':ash , 'maghrib':mgr , 'isha':ish , 'subuh':sbh }
    
})

$(".li-seragam").click( function(){
    let seragambar = $(this).children('div.form-check').children('input');;
    if( seragambar.prop('checked') == true){
        seragambar.prop('checked',false);
    }else{
        seragambar.prop('checked',true);
    }
})

$('#btnSeragam').click( function(){
    $('#seragamq').modal('toggle');
    let srg,rph,atr,sbk,ksk,data;
    srg = isceked("#cbSeragam");
    rph = isceked("#cbRapih");
    atr = isceked("#cbAttribut");
    sbk = isceked("#cbSabuk");
    ksk = isceked("#cbKauskaki");
    data = `${srg}-${rph}-${atr}-${ksk}-${sbk}`;
    localStorage.setItem('seragame' , data);    
})

$(".li-hadir").click( function(){
    let hadirbar = $(this).children('div.form-check').children('input');;
    if( hadirbar.prop('checked') == true){
        hadirbar.prop('checked',false);
    }else{
        hadirbar.prop('checked',true);
    }
})

$('#btnHadir').click( function(){
    $('#hadirq').modal('toggle');
    let hdr,lbt,data;
    hdr = isceked("#cbHadir");
    lbt = isceked("#cbTerlambat");
    data = `${hdr}-${lbt}`;
    localStorage.setItem('hadire' , data);    
})

$(".li-mood").click( function(){
    let moodbar = $(this).children('div.form-check').children('input');
    let data = moodbar.val();
    localStorage.setItem('moodq',data);
    if( moodbar.prop('checked') == true){
        moodbar.prop('checked',false);
    }else{
        moodbar.prop('checked',true);
    }
})

$('#caprisdone').click( function(){
    let post = { 'nis':localStorage.getItem('swnis'), 'sholat':localStorage.getItem('sholate') , 'seragam':localStorage.getItem('seragame'), 'presensi':localStorage.getItem('hadire'),'mood':localStorage.getItem('moodq') };
    $.post( pusdat + `saveCapris` , {
        data:post
    },function(resp){
        console.log(resp);
        localStorage.setItem('swnis','');
        localStorage.setItem('sholate','');
        localStorage.setItem('seragame','');
        localStorage.setItem('hadire','');
        localStorage.setItem('moodq','');
        $('input.form-check-input').prop('checked',false);
    })
})


function isceked(elmt){
    let iscek = $(elmt).prop('checked');
    if(iscek == true){ return 1 ; }else{ return 0; }
}


function wulanapa(n){
    let wulane = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','Nopember','Desember'];
    return wulane[n];
}