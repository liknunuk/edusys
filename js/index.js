var today = new Date();
var dina = today.getDate();
var sasi = today.getMonth();
var taun = today.getFullYear();
let wulan = wulanapa(sasi);

var kelas = '', absen = '';

var hariini =  dina + ' ' + wulan + ' ' + taun;
$("#hariini").html(hariini);
// dataKelas
$.getJSON(pusdat + `dataKelas` , function(kelas){
    $('#kelas').html('');
    $.each( kelas , function(i,data){
        $('#kelas').append(`
            <button class='btn btn-primary btn-kelas'>${data.kelas}</button>
        `);
    })
})

for( let i = 0 ; i < 40 ; i++ ){
    let abs = i+1;
    $('#absen').append(`
        <button class='btn btn-primary btn-absen'>${abs}</button>
    `);
}

$('#kelas').on( 'click' , 'button' , function(){
    kelas = $(this).text();
})

$('#absen').on( 'click' , 'button' , function(){
    absen = $(this).text();

    let info = "Kelas " + kelas + " No. Absen : " + absen;
    $.getJSON( pusdat + `siswaByKlAbs/${kelas}/${absen}` , function (siswa){
        $('.sisinfo').html('');
        $('#sisnis').html(siswa.nis);
        $('#sisnama').html(siswa.nama);
        $('#siskls').html(kelas);
        $('#sisabs').html(absen);

        localStorage.setItem('swnis',siswa.nis);
    })
})
$(".cbSholat").click( function(){
    let id=$(this).children('input').prop('id');
    setCheck(id);
})

$(".cbSeragam").click( function(){
    let id=$(this).children('input').prop('id');
    setCheck(id);
})


$('#caprisdone').click( function(){
    // cek sholat
    let dzu,ash,mgr,ish,sbh,data1;
    dzu = isceked("#cbDzuhur");
    ash = isceked("#cbAshar");
    mgr = isceked("#cbMaghrib");
    ish = isceked("#cbIsha");
    sbh = isceked("#cbSubuh");
    data1 = `${sbh}-${dzu}-${ash}-${mgr}-${ish}`;
    //localStorage.setItem('sholate' , data1);
    
    // cek seragam
    let srg,rph,atr,sbk,ksk,data2;
    srg = isceked("#cbSeragam");
    rph = isceked("#cbRapih"); /* diganti dasi */
    atr = isceked("#cbAttribut");
    sbk = isceked("#cbSabuk");
    ksk = isceked("#cbKauskaki");
    data2 = `${srg}-${rph}-${atr}-${ksk}-${sbk}`;
    //localStorage.setItem('seragame' , data2);

    // cek kehadiran
    let hdr,tlt,data3;
    hdr = isceked("#cbHadir");
    tlt = isceked("#cbTerlambat");
    data3 = `${hdr}-${tlt}`;
    //localStorage.setItem('hadire' , data3);
    let post = { 'nis':localStorage.getItem('swnis'), 'sholat':data1 , 'seragam':data2, 'presensi':data3, mood:'Optimis' };

    console.log(post);
    $.post( pusdat + `saveCapris` , {
        data:post
    },function(resp){
        console.log(resp);
        unchecked("#cbDzuhur");
        unchecked("#cbAshar");
        unchecked("#cbMaghrib");
        unchecked("#cbIsha");
        unchecked("#cbSubuh");
        unchecked("#cbSeragam");
        unchecked("#cbRapih"); /* diganti dasi */
        unchecked("#cbAttribut");
        unchecked("#cbSabuk");
        unchecked("#cbKauskaki");
        unchecked("#cbTerlambat");
        $("#siskls").html('');
        $("#sisabs").html('');
        $("#sisnama").html('');
        $("#sisnis").html('');
        localStorage.setItem('swnis','');
    })
 
    alert("Terima kasih. Data Sudah Disimpan");

})

function wulanapa(n){
    let wulane = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','Nopember','Desember'];
    return wulane[n];
}

function isceked(elmt){
    let iscek = $(elmt).prop('checked');
    if(iscek == true){ return 1 ; }else{ return 0; }
}

function unchecked(id){
    $(id).prop('checked',false);
}

function setCheck(id){
    if($("#"+id).prop('checked') == true ){
        $("#"+id).prop('checked',false);
    }else{
        $("#"+id).prop('checked',true);
        if(id == 'cbMens'){lagimen();}
    }
}

function lagimen(){
    $("#cbSubuh").prop('checked',false);
    $("#cbDzuhur").prop('checked',false);
    $("#cbAshar").prop('checked',false);
    $("#cbMaghrib").prop('checked',false);
    $("#cbIsha").prop('checked',false);
}