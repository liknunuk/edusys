<?php

class Controller {
  public function __construct(){
    $obj = explode("/",$_GET['url']);
    
    if(strtolower($obj[0]) != "pusdat"){
    $data['title'] = 'Bee Spirit';
    $this->view('template/header',$data);
    $this->view('template/navbar');} 
  }
  public function view($view, $data= [] )
  {
    require_once './app/views/'. $view . '.php';
  }

  public function model( $model )
  {
    require_once './app/models/'. $model . '.php';
    return new $model;
  }
}
