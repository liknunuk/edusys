<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="<?=BASEURL;?>">
    <img src="<?=BASEURL;?>/assets/img/pbbee.png" alt="smk pb rakit bee" style = "width:55px; height:55px; background-color: #EEEEEE; border-radius: 50%;">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="ddDaprim" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Data Primer
        </a>
        <div class="dropdown-menu" aria-labelledby="ddDaprim">
          <a class="dropdown-item" href="<?=BASEURL;?>/guru">Guru</a>
          <a class="dropdown-item" href="<?=BASEURL;?>/siswa">Siswa</a>
          <a class="dropdown-item" href="<?=BASEURL;?>/kelas">Kelas</a>
          <a class="dropdown-item" href="<?=BASEURL;?>/users">Pengguna</a>
        </div> 
      </li>
      
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="ddDaptrans" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Data Transaksi
        </a>
        <div class="dropdown-menu" aria-labelledby="ddDaptrans">
          <a class="dropdown-item" href="<?=BASEURL;?>/walikelas">Wali Kelas</a>
          <a class="dropdown-item" href="<?=BASEURL;?>/klsiswa">Kelas Siswa</a>
        </div>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="ddLaporan" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Laporan
        </a>
        <div class="dropdown-menu" aria-labelledby="ddLaporan">
          <a class="dropdown-item" href="<?=BASEURL;?>/laporan/presensi">Kehadiran</a>
          <a class="dropdown-item" href="<?=BASEURL;?>/laporan/ibadah">Ibadah</a>
          <a class="dropdown-item" href="<?=BASEURL;?>/laporan/seragam">Seragam</a>
          <a class="dropdown-item" href="<?=BASEURL;?>/laporan/semangat">Semangat</a>
          <a class="dropdown-item" href="<?=BASEURL;?>/laporan/capris">Raport Pribadi</a>
        </div>
      </li>

    </ul>
    <ul class="navbar-nav mr-0">
    <li class="nav-item">
        <a class="nav-link" href="#">
          <i class="fa fa-user"> User</i>
        </a>
      </li>
    <div>
  </div>
</nav>