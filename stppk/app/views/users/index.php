<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
        <form class="form-inline mt-3" method="POST" action="<?=BASEURL;?>/users/tambah">
        
            <div class="input-group mb-2 mr-sm-2">
                <div class="input-group-prepend">
                <div class="input-group-text">User name</div>
                </div>
                <input type="text" class="form-control" name="nisy" id="nisy" placeholder="NIS / NIY">
            </div>

            <div class="input-group mb-2 mr-sm-2">
                <div class="input-group-prepend">
                <div class="input-group-text">Group User</div>
                </div>
                <select class="form-control" name="usergroup" id="usergroup">
                    <option value="guru">Guru / Karyawan</option>
                    <option value="siswa">Siswa / Siswi</option>
                </select>
            </div>

            <div class="input-group mb-2 mr-sm-2">
                <div class="input-group-prepend">
                <div class="input-group-text">Password</div>
                </div>
                <input type="password" class="form-control" name="password" id="password" placeholder="Password">
            </div>

            <div class="input-group mb-2 mr-sm-2">
                <div class="input-group-prepend">
                <div class="input-group-text">Password</div>
                </div>
                <input type="password" class="form-control" name="passcheck" id="passcheck" placeholder="Ulangi Password">
            </div>

            <button type="submit" id='submit' class="btn btn-primary mb-2">Submit</button>
            </form>
    
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-striped table-sm">
                    <thead>
                        <tr class="bg-dark text-light">
                            <th>Username</th>
                            <th>Usergroup</th>
                            <th>Realname</th>
                            <th>
                                <i class="fas fa-cog">&nbsp;</i>
                            </th>
                        </tr>
                    </thead>
                    <tbody id="dp-users"></tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready( function(){
    $('#nisy').focus();
    $('#nisy').keypress( function(e){
        if( e.keyCode == 13 ){
            e.preventDefault();
            goto('usergroup');
        }
    });

    $('#nisy').change( function(){
        goto('password');
    });
        
    $('#password').keypress( function(e){
        if( e.keyCode == 13 ){
            e.preventDefault();
            goto('passcheck');
        }
    });

    $('#passcheck').keypress( function(e){
        if( e.keyCode == 13 ){
            e.preventDefault();
            if( $(this).val() == $('#password').val() ) {
                goto('#submit');
            }else{
                $('#password').val('');
                $('#passcheck').val('');
                alert('Password Tidak Sama');
                $('#password').focus();
            }
        }
    });
    

    listUser();

    $('#dp-users').on( 'click' , '.fa-trash' , function(){
        let tenan = confirm('Data User akan Dihapus');
        if(tenan == true){
            let nisy = $(this).parent('td').parent('tr').children('td:nth-child(1)').text(), 
                ugrp = $(this).parent('td').parent('tr').children('td:nth-child(2)').text();
            $.post("<?=BASEURL;?>/users/hapus" , { nisy : nisy , table : ugrp } , function(){
                listUser();
            })
        }
    })


})

function goto(dest){
    $('#'+dest).focus();
}

function listUser(){
    $.getJSON('<?=BASEURL;?>/pusdat/datauser' , function(users){
        $('#dp-users tr').remove();
        $.each(users, function(i,data){
            $('#dp-users').append(`
            <tr>
            <td>${data.uname}</td>
            <td>${data.ugroup}</td>
            <td>${data.realname}</td>
            <td>
                <i class='fa fa-trash' style='font-size: 14px; color: maroon; cursor:pointer;'> &nbsp;</i>
            </td>
            </tr>
            `);
        })
    })
}
</script>
