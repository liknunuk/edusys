<div class="container-fluid">
  <div class="row">
    <div class="col-md-3">
      <div class="card">
        <div class="card-header bg-primary">
          <h4 class='text-light'>Formulir Kelas</h4>
        </div>
        <div class="card-body">
          <form action="<?=BASEURL;?>/kelas/inedit" method="post">
            <input type="hidden" name="modkelas" id="modkelas" value="nambah">
            <div class="for-group">
              <label for="id">ID KELAS</label>
              <input type="text" name="id" id="id" class="form-control" readonly>
            </div>

            <div class="form-group">
              <label for="tingkat">Tingkat</label>
              <input type="text" name="tingkat" id="tingkat" class="form-control" list="lstTingkat" required>
              <datalist id="lstTingkat">
                <option value="X">X</option>
                <option value="XI">XI</option>
                <option value="XII">XII</option>
              </datalist>
            </div>

            <div class="form-group">
              <label for="jurusan">Program Kahlian</label>
              <input type="text" name="jurusan" id="jurusan" class="form-control" list="lstJurusan" required>
              <datalist id="lstJurusan">
              <option value="TKJ">TKJ</option>
              <option value="TKR">TKR</option>
              <option value="TSM">TSM</option>
              </datalist>
            </div>

            <div class="form-group">
              <label for="ruang">Urut Kelas</label>
              <input type="number" name="ruang" id="ruang" class="form-control" list="lstRuang" required>
              <datalist id="lstRuang">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
              </datalist>
            </div>

            <div class="form-group">
              <label for="kelas">Nama kelas</label>
              <input type="text" name="kelas" id="kelas" class="form-control" readonly>
            </div>
            <div class="form-group float-right">
              <button type="reset" class="btn btn-secondary">Reset</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-lg-9">
      <div class="card">
        <div class="card-header bg-success">
          <h3 class='text-light'>Daftar Siswa</h3>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-sm border border-dark">
              <thead>
                <tr class="bg-secondary text-light text-center">
                  <th>ID</th>
                  <th>Nama Kelas</th>
                  <th>Tingkat</th>
                  <th>Jurusan</th>
                  <th>Urut Kelas</th>
                  <th width="80px;">
                    <i class="fas fa-cog"></i>
                  </th>
                </tr>
              </thead>
              <!-- dp = data primer -->
              <tbody id="dp-kelas"></tbody>
            </table>
            <!-- alert -->
            <div id="alert"></div>
              
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready( function(){

  //tampilkan guru
  $.getJSON( `<?=BASEURL;?>/pusdat/dataKelas` , function(guru){
    $('#dp-kelas tr').remove();
    $.each( guru , function( i , data ){
      $('#dp-kelas').append(`
        <tr class='text-center'>
        <td>${data.id}</td>
        <td>${data.kelas}</td>
        <td>${data.tingkat}</td>
        <td>${data.jurusan}</td>
        <td>${data.ruang}</td>
        <td class="text-center">
          <i class="far fa-edit" style="font-size:20px;">&nbsp;</i>
          <i class="fa fa-trash" style="font-size:20px;">&nbsp;</i>
        </td>
        </tr>
      `);
    })    
  });

  // edit kelas
  $('#dp-kelas').on( ' click ' , ' .fa-edit ' , function(){
    let id,kelas,tingkat,jurusan,ruang;
    id = $(this).parent('td').parent('tr').children('td:nth-child(1)').text();
    kelas = $(this).parent('td').parent('tr').children('td:nth-child(2)').text();
    tingkat = $(this).parent('td').parent('tr').children('td:nth-child(3)').text();
    jurusan = $(this).parent('td').parent('tr').children('td:nth-child(4)').text();
    ruang = $(this).parent('td').parent('tr').children('td:nth-child(5)').text();

    $('#id').val(id);
    $('#kelas').val(kelas);
    $('#tingkat').val(tingkat);
    $('#jurusan').val(jurusan);
    $('#ruang').val(ruang);
    $('#modkelas').val('ngubah');

  })
  // hapus kelas

  $('#dp-kelas').on( ' click ' , ' .fa-trash ' ,  function(){
    let id = $(this).parent('td').parent('tr').children('td:nth-child(1)').text();
    var saestu = confirm('Data Kelas Akan Dihapus');
    if( saestu == true ){
      $.post(`<?=BASEURL?>/kelas/gusur`,{id:id},function(){
        let alert = `
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>Berhasil !</strong> Data siswa telah dihapus.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick=location.reload()>
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        `;
        $('#alert').html(alert);
        setTimeout(reload, 1500)
      });
    }
  })

  $('#ruang').change( function(){
      let tk,jr,rg, kl;
      tk = $('#tingkat').val();
      jr = $('#jurusan').val();
      rg = $('#ruang').val();
      kl = tk+'-'+jr+'-'+rg;
      $("#kelas").val(kl);
  })

})

function reload(){
  location.reload();
}
</script>

