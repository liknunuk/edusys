<div class="container-fluid">
  <div class="row">
    <div class="col-md-3">
      <div class="card">
        <div class="card-header bg-primary">
          <h4 class='text-light'>Formulir Wali Kelas</h4>
        </div>
        <div class="card-body">
          <form action="<?=BASEURL;?>/walikelas/inedit" method="post">
            <input type="hidden" name="modwali" id="modwali" value="nambah">
            <div class="for-group">
              <label for="id">ID</label>
              <input type="text" name="id" id="id" class="form-control" readonly>
            </div>

            <div class="form-group">
              <label for="tapel">Tahun Pelajaran</label>
              <input type="text" name="tapel" id="tapel" class="form-control" value='2019/2020' required>
            </div>

            <div class="form-group">
              <label for="niy">NIY/NIP Guru</label>
              <input type="text" name="niy" id="niy" class="form-control" list="lstniy" placeholder = "Tulis Nama Guru">
              <datalist id="lstniy"></datalist>
            </div>

            <div class="form-group">
              <label for="kelas">ID Kelas</label>
              <input type="text" name="kelas" id="kelas" class="form-control" list="lstkelas" placeholder = "Tulis Nama Kelas">
              <datalist id="lstkelas"></datalist>
            </div>

            <div class="form-group float-right">
              <button type="reset" class="btn btn-secondary">Reset</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-lg-9">
      <div class="card">
        <div class="card-header bg-success">
          <h3 class='text-light'>Daftar Wali Kelas</h3>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-sm border border-dark">
              <thead>
                <tr class="bg-secondary text-light text-center">
                  <th>ID</th>
                  <th>Tahun Pelajaran</th>
                  <th>Kelas</th>
                  <th>Wali</th>
                  <th width="80px;">
                    <i class="fas fa-cog"></i>
                  </th>
                </tr>
              </thead>
              <!-- dp = data primer -->
              <tbody id="dt-wali"></tbody>
            </table>
            <!-- alert -->
            <div id="alert"></div>
              
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready( function(){

  //tampilkan guru
  $.getJSON( `<?=BASEURL;?>/pusdat/dataWalikelas` , function(guru){
    $('#dt-wali tr').remove();
    $.each( guru , function( i , data ){
      $('#dt-wali').append(`
        <tr class='text-center'>
        <td>${data.id}</td>
        <td>${data.tapel}</td>
        <td>${data.wali}<br/>[ <span id='wkniy'>${data.niy}</span> ]</td>
        <td>${data.nmkelas} [ <span id='wkidkls'>${data.kelas}</span> ]</td>
        <td class="text-center">
          <i class="far fa-edit" style="font-size:20px;">&nbsp;</i>
          <i class="fa fa-trash" style="font-size:20px;">&nbsp;</i>
        </td>
        </tr>
      `);
    })    
  });

  // edit kelas
  $('#dt-wali').on( ' click ' , ' .fa-edit ' , function(){
    let id,tapel,niy,kelas;
    id = $(this).parent('td').parent('tr').children('td:nth-child(1)').text();
    tapel = $(this).parent('td').parent('tr').children('td:nth-child(2)').text();
    niy = $(this).parent('td').parent('tr').children('td:nth-child(3)').children('span#wkniy').text();
    kelas = $(this).parent('td').parent('tr').children('td:nth-child(4)').children('span#wkidkls').text();

    $('#id').val(id);
    $('#tapel').val(tapel);
    $('#niy').val(niy);
    $('#kelas').val(kelas);
    $('#modwali').val('ngubah');

  })
  // hapus kelas

  $('#dt-wali').on( ' click ' , ' .fa-trash ' ,  function(){
    let id = $(this).parent('td').parent('tr').children('td:nth-child(1)').text();
    var saestu = confirm('Data Wali Kelas Akan Dihapus');
    if( saestu == true ){
      $.post(`<?=BASEURL?>/walikelas/pensiun`,{id:id},function(){
        let alert = `
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>Berhasil !</strong> Data wali kelas telah dihapus.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick=location.reload()>
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        `;
        $('#alert').html(alert);
        setTimeout(reload, 1500)
      });
    }
  })

  $('#niy').keyup( function(){
    let nama = $(this).val();
    if( nama.length >=4 ){
        $.getJSON(`<?=BASEURL;?>/pusdat/gurubyname/${nama}` , function(guru){
            $('#lstniy option').remove();
            $.each( guru , function( i , data ){
                $('#lstniy').append(`<option value=${data.niy}>${data.nama}</option>`);
            })
        })
    }
  });

  $('#kelas').keyup( function(){
    let kelas = $(this).val();
    if( kelas.length >=1 ){
        $.getJSON(`<?=BASEURL;?>/pusdat/kelasbyname/${kelas}` , function(guru){
            $('#lstkelas option').remove();
            $.each( guru , function( i , data ){
                $('#lstkelas').append(`<option value=${data.id}>${data.kelas}</option>`);
            })
        })
    }
  });
})



function reload(){
  location.reload();
}
</script>

