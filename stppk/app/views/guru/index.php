<div class="container-fluid">
  <div class="row">
    <div class="col-md-3">
      <div class="card">
        <div class="card-header bg-primary">
          <h4 class='text-light'>Formulir Guru</h4>
        </div>
        <div class="card-body">
          <form action="<?=BASEURL;?>/guru/inedit" method="post">
            <input type="hidden" name="modguru" id="modguru" value="nambah">
            <div class="for-group">
              <label for="nipy">NIY / NIP</label>
              <input type="text" name="nipy" id="nipy" class="form-control">
            </div>

            <div class="form-group">
              <label for="nama">Nama Guru</label>
              <input type="text" name="nama" id="nama" class="form-control">
            </div>

            <div class="form-group">
              <label for="hp">Nomor HP</label>
              <input type="text" name="hp" id="hp" class="form-control">
            </div>
            <div class="form-group float-right">
              <button type="reset" class="btn btn-secondary">Reset</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-lg-9">
      <div class="card">
        <div class="card-header bg-success">
          <h3 class='text-light'>Daftar Guru</h3>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-sm border border-dark">
              <thead>
                <tr class="bg-secondary text-light text-center">
                  <th>NIP / NIY</th>
                  <th>Nama Lengkap</th>
                  <th>No. HP</th>
                  <th width="80px;">
                    <i class="fas fa-cog"></i>
                  </th>
                </tr>
              </thead>
              <!-- dp = data primer -->
              <tbody id="dp-guru"></tbody>
            </table>
            <!-- alert -->
            <div id="alert"></div>
              
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready( function(){

  //tampilkan guru
  $.getJSON( `<?=BASEURL;?>/pusdat/dataGuru` , function(guru){
    $('#dp-guru tr').remove();
    $.each( guru , function( i , data ){
      $('#dp-guru').append(`
        <tr>
        <td>${data.niy}</td>
        <td>${data.nama}</td>
        <td>${data.hp}</td>
        <td class="text-center">
          <i class="far fa-edit" style="font-size:20px;">&nbsp;</i>
          <i class="fa fa-trash" style="font-size:20px;">&nbsp;</i>
        </td>
        </tr>
      `);
    })    
  });

  // edit guru
  $('#dp-guru').on( ' click ' , ' .fa-edit ' , function(){
    let niy,nama,hp;
    niy = $(this).parent('td').parent('tr').children('td:nth-child(1)').text();
    nama = $(this).parent('td').parent('tr').children('td:nth-child(2)').text();
    hp = $(this).parent('td').parent('tr').children('td:nth-child(3)').text();

    $('#nipy').val(niy);
    $('#nipy').prop('readonly','readonly');
    $('#nama').val(nama);
    $('#hp').val(hp);
    $('#modguru').val('ngubah');

  })
  // hapus guru

  $('#dp-guru').on( ' click ' , ' .fa-trash ' ,  function(){
    let niy = $(this).parent('td').parent('tr').children('td:nth-child(1)').text();
    let bar = $(this).parent('td').parent('tr');
    var saestu = confirm('Data Guru Akan Dihapus');
    if( saestu == true ){
      bar.css('display:none');
      $.post(`<?=BASEURL?>/guru/retire`,{nipy:niy},function(){
        let alert = `
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>Berhasil !</strong> Data guru telah dihapus.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick=location.reload()>
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        `;
        $('#alert').html(alert);
        setTimeout(reload, 1500)
      });
    }
  })

})

function reload(){
  location.reload();
}
</script>

