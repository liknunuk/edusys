<div class="container-fluid">
    <div class="row mt-3">
        <div class="col-md-3">
            <form>
                <div class="form-group row">
                    <label for="tgfilter" class="col-sm-3">Tanggal</label>
                    <div class="col-sm-9">
                    <input type="date" class='form-control' id="tgfilter">
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-3">
            <form>
                <div class="form-group row">
                    <label for="klfilter" class="col-sm-3">Kelas</label>
                    <div class="col-sm-9">
                    <select id="klfilter" class="form-control"></select>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-3">&nbsp;</div>
    </div>
    <div class="row">
        <div class="col-md-12 mt-3">
            <div class="card">
                <div class="card-header">
                <h3>LAPORAN SERAGAM SISWA KELAS <span id="klhadir"></span> TANGGAL <span id="tghadir"></span></h3>
                </div>
                <div class="card-body">
                    <table class="table table-sm">
                        <thead>
                            <tr>
                                <th>Nama Siswa</th>
                                <th width='150'>Seragam</th>
                                <th width='150'>Kerapihan</th>
                                <th width='150'>Attribut</th>
                                <th width='150'>Kaus Kaki</th>
                                <th width='150'>Sabuk</th>
                            </tr>
                        </thead>
                        <tbody id="laporan"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$.getJSON(`<?=BASEURL;?>/pusdat/dataKelas` , function(kelas){
    $("#klfilter option").remove();
    $("#klfilter").append('<option>Pilih Kelas</option>');
    $.each(kelas , function(i,data){
        $("#klfilter").append(`<option value='${data.id}'>${data.kelas}</option>`);
    })
})

$('#tgfilter').change( function(){
    let tgl = $(this).val().split('-');
    let tglabel = tgl[2]+'/'+tgl[1]+'/'+tgl[0];
    $('#tghadir').html(tglabel);
})

$("#klfilter").change( function(){
    $("#klhadir").text($('#klfilter option:selected').text());
    let kls = $(this).val(),
        tgl = $("#tgfilter").val();
    if( kls !== 'Pilih Kelas'){
        $.getJSON(`<?=BASEURL;?>/pusdat/dataSeragam/${tgl}/${kls}` , function(hadir){
            $("#laporan tr").remove();
            $.each( hadir, function(i,data){
                $("#laporan").append(`
                <tr>
                <td>${data.nama}</td>
                <td>${data.baju}</td>
                <td>${data.rapi}</td>
                <td>${data.attr}</td>
                <td>${data.sock}</td>
                <td>${data.belt}</td>
                </tr>
                `);
            })
        })
    }
})
</script>