<div class="container-fluid">
    <div class="row">
        <div class="col-lg-4">
            <div class="card">
                <div class="card-header bg-primary">
                    <h4 class='text-light'>Formulir Kelas Siswa</h4>
                </div>
                <div class="card-body">
                    <form rule="form" id="fklsiwa">
                        <div class="form-group">
                            <label for="tapel">Tahun Pelajaran</label>
                            <input type="text" value="<?= tahunajaran; ?>" id="tapel" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="kelas">Pilih Kelas</label>
                            <select id="kelas" class="form-control"></select>
                        </div>

                        <div class="form-group">
                            <label for="absen">Nomor Absen</label>
                            <input type="number" min="1" max="50" id="absen" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="siswa">Nama Siswa</label>
                            <input type="text" id="siswa" class="form-control" placeholder="Cari Nama Siswa" list = "lstSiswa"><datalist id="lstSiswa"></datalist>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="card">
                <div class="card-header bg-success text-light">
                    <h4>DAFTAR SISWA KELAS <span id="klsiswa"></span> TAHUN PELAJARAN <?= tahunajaran ; ?></h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-sm">
                            <thead>
                                <tr class="bg-dark text-light text-center">
                                    <th width="80">Nomor</th>
                                    <th>NIS</th>
                                    <th>Nama Siswa</th>
                                    <th width="50">ID</th>
                                    <th width="80">
                                        <i class="fas fa-cog"></i>
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="dt-klsiswa"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready( function(){
    // ambil data kelas default
    dftSiswa('<?=tahunajaran;?>',1);
    // data kelas
    $.getJSON( '<?=BASEURL;?>/pusdat/dataKelas' , function(kelas){
        $('#kelas option').remove();
        $.each( kelas , function(i,data){
            $('#kelas').append(`<option value='${data.id}'>${data.kelas}`)
        })
    })

    // sematkan nama kelas di judul daftar kelas
    $('#kelas').change( function(){
        let nmkelas = $("#kelas option:selected").text();
        $("#klsiswa").text(nmkelas);
        dftSiswa('<?=tahunajaran;?>',$(this).val());
    })

    //siswa by nama
    $('#siswa').keyup( function(){
        let nama = $(this).val();
        if( nama.length >= 4 ){
            $.getJSON(`<?=BASEURL;?>/pusdat/siswabyname/${nama}` , function(siswa){
                $('#lstSiswa option').remove();
                $.each( siswa , function(i,data){
                    $('#lstSiswa').append(`<option value=${data.nis}>${data.nama}</option>`)
                })
            })
        }
    })

    // submit ke basis data
    $('#siswa').keypress( function(evt){
        if(evt.keyCode == 13){
            evt.preventDefault();
            let tpl,kls,nis,abs;
            tpl = $('#tapel').val();
            kls = $('#kelas').val();
            abs = $('#absen').val();
            nis = $('#siswa').val();
            
            $.post('<?=BASEURL;?>/klsiswa/tambah' , {
                tpl : tpl , kls : kls , nis : nis , abs : abs
            },function(resp){
                // bersihkan form nis
                $('#siswa').val('');

                // tampilkan daftar siswa tapel xx/yy kelas zz
                dftSiswa(tpl,kls);
            })
        }
    })

    // hapus data siswa
    $('#dt-klsiswa').on( 'click' , '.fa-trash' , function(){
        let id = $(this).parent('td').parent('tr').children('td:nth-child(4)').text(),
            sure = confirm("Siswa Dihapus Dari Daftar Kelas !!!");
        if( sure == true ){
            $.post(`<?=BASEURL;?>/klsiswa/mutasi` , { id: id } , function(resp){
                let tpl = $('#tapel').val(), 
                    kls = $('#kelas').val();
                    
                dftSiswa(tpl,kls)
            })
        }

    })
})

function dftSiswa(tpl,kls){
    $.getJSON(`<?=BASEURL;?>/pusdat/dataSiswaKelas/${tpl}/${kls}` , function(siswa){
        $('#dt-klsiswa tr').remove();
        $.each( siswa , function(i,data){
            $('#dt-klsiswa').append(`
                <tr>
                <td>${data.absen}</td>
                <td>${data.nis}</td>
                <td>${data.nama}</td>
                <td>${data.id}</td>
                <td class='text-center'><i class='fa fa-trash' style='font-size:14pt;'></i>
                </tr>
            `);
        });
    })
}
</script>