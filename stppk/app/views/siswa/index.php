<div class="container-fluid">
  <div class="row">
    <div class="col-md-3">
      <div class="card">
        <div class="card-header bg-primary">
          <h4 class='text-light'>Formulir Kelas</h4>
        </div>
        <div class="card-body">
          <form action="<?=BASEURL;?>/siswa/inedit" method="post">
            <input type="hidden" name="modsiswa" id="modsiswa" value="nambah">
            <div class="for-group">
              <label for="nis">NIS</label>
              <input type="text" name="nis" id="nis" class="form-control">
            </div>

            <div class="form-group">
              <label for="nama">Nama Siswa</label>
              <input type="text" name="nama" id="nama" class="form-control" required>
            </div>

            <div class="form-group">
              <label for="ortu">Nama Orang Tua</label>
              <input type="text" name="ortu" id="ortu" class="form-control" required>
            </div>

            <div class="form-group">
              <label for="alamat">Alamat Tinggal</label>
              <input type="text" name="alamat" id="alamat" class="form-control" required>
            </div>

            <div class="form-group">
              <label for="hp">Nomor HP</label>
              <input type="text" name="hp" id="hp" class="form-control">
            </div>
            
            <div class="form-group">
              <label for="sandi">Password</label>
              <input type="text" name="sandi" id="sandi" class="form-control" maxlength='4' value="<?php randomPassword(); ?>" >
            </div>

            <div class="form-group float-right">
              <button type="reset" class="btn btn-secondary">Reset</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-lg-9">
      <div class="card">
        <div class="card-header bg-success">
          <h3 class='text-light'>Daftar Siswa</h3>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-sm border border-dark">
              <thead>
                <tr class="bg-secondary text-light text-center">
                  <th>NIS</th>
                  <th>Nama Lengkap</th>
                  <th>Nama Orang Tua</th>
                  <th>Alamat</th>
                  <th>No. HP</th>
                  <th width="80px;">
                    <i class="fas fa-cog"></i>
                  </th>
                </tr>
              </thead>
              <!-- dp = data primer -->
              <tbody id="dp-siswa"></tbody>
            </table>
            <!-- alert -->
            <div id="alert"></div>
              
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready( function(){

  //tampilkan guru
  $.getJSON( `<?=BASEURL;?>/pusdat/dataSiswa` , function(guru){
    $('#dp-siswa tr').remove();
    $.each( guru , function( i , data ){
      $('#dp-siswa').append(`
        <tr>
        <td>${data.nis}</td>
        <td>${data.nama}</td>
        <td>${data.ortu}</td>
        <td>${data.alamat}</td>
        <td>${data.hp}</td>
        <td class="text-center">
          <i class="far fa-edit" style="font-size:20px;">&nbsp;</i>
          <i class="fa fa-trash" style="font-size:20px;">&nbsp;</i>
        </td>
        </tr>
      `);
    })    
  });

  // edit guru
  $('#dp-siswa').on( ' click ' , ' .fa-edit ' , function(){
    let niy,nama,hp;
    nis = $(this).parent('td').parent('tr').children('td:nth-child(1)').text();
    nama = $(this).parent('td').parent('tr').children('td:nth-child(2)').text();
    ortu = $(this).parent('td').parent('tr').children('td:nth-child(3)').text();
    alamat = $(this).parent('td').parent('tr').children('td:nth-child(4)').text();
    hp = $(this).parent('td').parent('tr').children('td:nth-child(5)').text();

    $('#nis').val(nis);
    $('#nis').prop('readonly','readonly');
    $('#nama').val(nama);
    $('#ortu').val(ortu);
    $('#alamat').val(alamat);
    $('#hp').val(hp);
    $('#modsiswa').val('ngubah');

  })
  // hapus guru

  $('#dp-siswa').on( ' click ' , ' .fa-trash ' ,  function(){
    let nis = $(this).parent('td').parent('tr').children('td:nth-child(1)').text();
    var saestu = confirm('Data Siswa Akan Dihapus');
    if( saestu == true ){
      $.post(`<?=BASEURL?>/siswa/alumnus`,{nis:nis},function(){
        let alert = `
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>Berhasil !</strong> Data siswa telah dihapus.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close" onclick=location.reload()>
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        `;
        $('#alert').html(alert);
        setTimeout(reload, 1500)
      });
    }
  })

})

function reload(){
  location.reload();
}
</script>

<?php

  function randomPassword(){
    $char1 = array('P','B','R','A','K','I','T');
    $char2 = array('t','i','k','a','r','b','p');
    $angka = array('0','1','2','3','4','5','6','7','8','9');

    $rc1 = rand(0,6); $rc2 = rand(0,6); $ra1 = rand(0,9); $ra2 = rand(0,9);
    echo $char1[$rc1].$angka[$ra1].$char2[$rc2].$angka[$ra2];

  }

?>