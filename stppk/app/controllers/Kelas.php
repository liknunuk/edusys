<?php

class Kelas extends Controller
{
  // method default
  public function index()
  {
    $data['title']="Kelas";

    /*$this->view('template/header',$data);
    $this->view('template/navbar');*/
    $this->view('kelas/index');
    $this->view('template/footer');
  }

  public function inedit(){
    if($_POST['modkelas'] == 'nambah'){

      if( $this->model('Model_kelas')->addKelas($_POST) > 0 )
      header("Location: " . BASEURL . "/kelas");

    }elseif($_POST['modkelas'] == 'ngubah'){

      if( $this->model('Model_kelas')->chgKelas($_POST) > 0 )
      header("Location: " . BASEURL . "/kelas");

    }else{

      header("Location: " . BASEURL . "/kelas");

    }
    
  }

  public function gusur(){

    if( $this->model('Model_kelas')->rmvKelas($_POST) > 0 )
      echo '
      
      ';

  }
}
