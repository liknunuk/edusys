<?php

class Pusdat extends Controller
{
  // method default
  public function index()
  {
    echo "Pusat Data Sistem Panca Bhakti Bee-Students";
  }

  // Data Guru
  public function dataGuru(){
      $this->model('Model_guru')->dataGuru();
  }

  public function gurubyname($name){
    $this->model('Model_guru')->caribyname($name);
  }

  // Data Siswa
  public function dataSiswa(){
      $this->model('Model_siswa')->dataSiswa();
  }

  public function siswabyname($nama){
      $this->model('Model_siswa')->caribyname($nama);
  }


  // Data Kelas
  public function dataKelas(){
      $this->model('Model_kelas')->dataKelas();
  }

  public function kelasbyname($name){
    $this->model('Model_kelas')->caribyname($name);
  }

  // Data Walikelas
  public function dataWalikelas(){
    $this->model('Model_walikelas')->dataWalikelas();
  }

  // Data Siswa Kelas
  public function dataSiswaKelas($ta,$kl){
    $this->model('Model_klsiswa')->klSiswa($ta,$kl);
  }

  // siswa menurut kelas dan absen
  public function siswaByKlAbs($kls,$abs){
    $this->model('Model_klsiswa')->siswaKlabs($kls,$abs);
  }

  // Data pengguna
  public function datauser(){
    $this->model('Model_users')->dataUsers();
  }

  // entry catatan siwa
  public function saveCapris(){
    print_r($_POST);
    /*
    Array
    (
    [data] => Array
        (
            [nis] => 18-2-0850
            [sholat] => 0-1-1-1-1
            [seragam] => 1-1-0-1-0
            [presensi] => 1-1
            [moo] => Optimis
        )
    )
     */
    
    // data sholat
    list($subuh,$dzuhur,$ashar,$maghrib,$isha)=explode('-',$_POST['data']['sholat']);
    $sholat = array('nis'=>$_POST['data']['nis'] ,'subuh'=>$subuh , 'dzuhur'=> $dzuhur , 'ashar' => $ashar , 'maghrib' => $maghrib , 'isha' => $isha );

    $this->model('Model_capris')->saveSholat($sholat);

    // data presensi
    list($hadir,$telat) = explode('-' , $_POST['data']['presensi']);
    $presensi = array('nis'=>$_POST['data']['nis'] , 'hadir' => $hadir , 'telat' => $telat );

    $this->model('Model_capris')->saveHadir($presensi);

    // data seragam ${srg}-${rph}-${atr}-${ksk}-${sbk}
    list ($pakaian,$rapi,$attribut,$kauskaki,$sabuk) = explode( '-' , $_POST['data']['seragam'] );
    $seragam = array ('nis'=>$_POST['data']['nis'] , 'seragam'=>$pakaian , 'rapi'=>$rapi, 'attribut'=>$attribut , 'kauskaki'=>$kauskaki , 'sabuk'=>$sabuk ) ;
    $this->model('Model_capris')->saveSeragam($seragam);

    // data mood
    $mood = array( 'nis'=>$_POST['data']['nis'] , 'mood'=>$_POST['data']['mood']);
    $this->model('Model_capris')->saveMood($mood);
    
  }
  
  public function dataPresensi($tgl,$kls){
   $this->model('Model_laporan')->presensi($tgl,$kls);
  }

  public function dataIbadah($tgl,$kls){
   $this->model('Model_laporan')->ibadah($tgl,$kls);
  }

  public function dataMood($tgl,$kls){
   $this->model('Model_laporan')->mood($tgl,$kls);
  }

  public function dataSeragam($tgl,$kls){
   $this->model('Model_laporan')->seragam($tgl,$kls);
  }


}
