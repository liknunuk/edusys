<?php

class Users extends Controller
{
    public function index(){
        $this->view('users/index');
        $this->view('template/footer');
    }

    public function tambah(){
        if( $this->model('Model_users')->tambahUser($_POST) > 0 )  {
            echo "
                <script>
                    window.location = '" . BASEURL. "/users';
                </script>
            ";
        }
    }

    public function hapus(){
        $this->model('Model_users')->hapusUser($_POST);
    }
}