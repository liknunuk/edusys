<?php

class Guru extends Controller
{
  // method default
  public function index()
  {
    $data['title']="Guru";

    /*$this->view('template/header',$data);
    $this->view('template/navbar');*/
    $this->view('guru/index');
    $this->view('template/footer');
  }

  public function inedit(){
    if($_POST['modguru'] == 'nambah'){

      if( $this->model('Model_guru')->addGuru($_POST) > 0 )
      header("Location: " . BASEURL . "/guru");

    }elseif($_POST['modguru'] == 'ngubah'){

      if( $this->model('Model_guru')->chgGuru($_POST) > 0 )
      header("Location: " . BASEURL . "/guru");

    }else{

      header("Location: " . BASEURL . "/guru");

    }
    
  }

  public function retire(){

    if( $this->model('Model_guru')->rmvGuru($_POST) > 0 )
      echo '
      
      ';

  }
}
