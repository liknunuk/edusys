<?php

class Siswa extends Controller
{
  // method default
  public function index()
  {
    $data['title']="Siswa";

    /*$this->view('template/header',$data);
    $this->view('template/navbar');*/
    $this->view('siswa/index');
    $this->view('template/footer');
  }

  public function inedit(){
    $data = array('nisy'=>$_POST['nis'],'usergroup'=>'siswa','password'=>$_POST['sandi']);
    if($_POST['modsiswa'] == 'nambah'){
      if( $this->model('Model_siswa')->addSiswa($_POST) > 0 && $this->model('Model_users')->tambahUser($data) > 0 )

      // header("Location: " . BASEURL . "/siswa");
      echo "<a href='". BASEURL . "/siswa' class='btn btn-success'>Kembali</a>";

    }elseif($_POST['modsiswa'] == 'ngubah'){

      if( $this->model('Model_siswa')->chgSiswa($_POST) > 0 && $this->model('Model_users')->ngubahUser($data) > 0 )
      //header("Location: " . BASEURL . "/siswa");
      echo "<a href='". BASEURL . "/siswa' class='btn btn-success'>Kembali</a>";

    }else{

      //header("Location: " . BASEURL . "/siswa");
      echo "<a href='". BASEURL . "/siswa' class='btn btn-success'>Kembali</a>";

    }
    
  }

  public function alumnus(){

    if( $this->model('Model_siswa')->rmvSiswa($_POST) > 0 )
      echo '
      
      ';

  }
}
