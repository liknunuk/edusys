<?php

class Laporan extends Controller
{
  // method default
  public function index()
  {
    $data['title']="Laporan";

    /*$this->view('template/header',$data);
    $this->view('template/navbar');*/
    $this->view('laporan/index');
    $this->view('template/footer');
  }

  public function presensi(){
    $this->view('laporan/presensi');
    $this->view('template/footer');
  }

  public function ibadah(){
    $this->view('laporan/ibadah');
    $this->view('template/footer');
  }

  public function semangat(){
    $this->view('laporan/mood');
    $this->view('template/footer');
  }

  public function seragam(){
    $this->view('laporan/seragam');
    $this->view('template/footer');
  }

}