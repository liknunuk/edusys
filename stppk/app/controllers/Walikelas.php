<?php

class Walikelas extends Controller
{
  // method default
  public function index()
  {
    $data['title']="Wali Kelas";

    /*$this->view('template/header',$data);
    $this->view('template/navbar');*/
    $this->view('walikelas/index');
    $this->view('template/footer');
  }

  public function inedit(){
    if($_POST['modwali'] == 'nambah'){

      if( $this->model('Model_walikelas')->addWali($_POST) > 0 )
      header("Location: " . BASEURL . "/walikelas");

    }elseif($_POST['modwali'] == 'ngubah'){

      if( $this->model('Model_walikelas')->chgWali($_POST) > 0 )
      header("Location: " . BASEURL . "/walikelas");

    }else{

      header("Location: " . BASEURL . "/walikelas");

    }
    
  }

  public function pensiun(){

    if( $this->model('Model_walikelas')->rmvWali($_POST) > 0 )
      echo '
      
      ';

  }
}
