<?php
class Model_walikelas
{
    public function __construct()
    {
        $this->db = new Database();
    }

    public function addWali($data){
        //print_r($data); 
        $sql = 'INSERT INTO walikls VALUES (:id,:tapel,:niy,:kelas)';
        $this->db->query($sql);
        
        $this->db->bind('id',NULL);
        $this->db->bind('tapel',$data['tapel']);
        $this->db->bind('niy',$data['niy']);
        $this->db->bind('kelas',$data['kelas']);

        $this->db->execute();

        return $this->db->rowCount();
  }

  public function chgWali($data){
    //print_r($data);
    $sql = 'UPDATE walikls SET tapel = :tapel , niy = :niy , kelas= :kelas WHERE id=:id';
    $this->db->query($sql);

    $this->db->bind('tapel',$data['tapel']);
    $this->db->bind('niy',$data['niy']);
    $this->db->bind('kelas',$data['kelas']);
    $this->db->bind('id',$data['id']);

    $this->db->execute();

    return $this->db->rowCount();
  }

  public function rmvWali($data){
    //print_r($data);
    $sql = 'DELETE FROM walikls WHERE id = :id ';
    $this->db->query($sql);

    $this->db->bind('id',$data['id']);

    $this->db->execute();

    return $this->db->rowCount();
  }

  public function dataWalikelas(){
    $sql = "SELECT walikls.*, guru.nama wali, kelas.kelas nmkelas from walikls,guru,kelas where guru.niy = walikls.niy && kelas.id=walikls.kelas && tapel = :tapel ";
    $this->db->query($sql);
    $this->db->bind('tapel',tahunajaran);
    $wali = $this->db->resultSet();
    echo json_encode($wali);
  }
}
