<?php
class Model_guru
{
    public function __construct()
    {
        $this->db = new Database();
    }

    public function addGuru($data){
        //print_r($data);
        $sql = 'INSERT INTO guru VALUES (:niy , :nama, :hp )';
        $this->db->query($sql);

        $this->db->bind('niy',$data['nipy']);
        $this->db->bind('nama',$data['nama']);
        $this->db->bind('hp',$data['hp']);

        $this->db->execute();

        return $this->db->rowCount();
  }

  public function chgGuru($data){
    //print_r($data);
    $sql = 'UPDATE guru SET nama = :nama, hp = :hp WHERE NIY = :niy ';
    $this->db->query($sql);

    $this->db->bind('nama',$data['nama']);
    $this->db->bind('hp',$data['hp']);
    $this->db->bind('niy',$data['nipy']);

    $this->db->execute();

    return $this->db->rowCount();
  }

  public function rmvGuru($data){
    //print_r($data);
    $sql = 'DELETE FROM guru WHERE NIY = :niy ';
    $this->db->query($sql);

    $this->db->bind('niy',$data['nipy']);

    $this->db->execute();

    return $this->db->rowCount();
  }

  public function dataGuru(){
      $sql = "SELECT * FROM guru ORDER BY nama LIMIT " . baris;
      $this->db->query($sql);
      $guru = $this->db->resultSet();
      echo json_encode($guru);
  }

  public function caribyname($name){
    $sql = "SELECT * FROM guru WHERE nama LIKE :nama ";
    $this->db->query($sql);

    $this->db->bind('nama','%'.$name.'%');

    $guru = $this->db->resultSet();
    
    echo json_encode($guru);
  }
}
