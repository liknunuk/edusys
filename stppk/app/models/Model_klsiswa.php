<?php
class Model_klsiswa
{
    public function __construct()
    {
        $this->db = new Database();
    }

    public function addKlsiswa($data){
        //print_r($data);
        $sql = 'INSERT INTO klsiswa VALUES (:id , :tapel , :nis , :kelas , :absen
        )';
        $this->db->query($sql);
        
        $this->db->bind('id',NULL);
        $this->db->bind('tapel',$data['tpl']);
        $this->db->bind('nis',$data['nis']);
        $this->db->bind('kelas',$data['kls']);
        $this->db->bind('absen',$data['abs']);

        $this->db->execute();

        return $this->db->rowCount();
        
  }

  public function chgKlsiswa($data){
    //print_r($data);
    $sql = 'UPDATE klsiswa SET tapel = :tapel , nis = :nis , absen = :absen , kelas= :kelas WHERE id=:id';
    $this->db->query($sql);

    $this->db->bind('tapel',$data['tpl']);
    $this->db->bind('nis',$data['nis']);
    $this->db->bind('kelas',$data['kls']);
    $this->db->bind('absen',$data['abs']);
    $this->db->bind('id',$data['id']);

    $this->db->execute();

    return $this->db->rowCount();
  }

  public function rmvKlsiswa($data){
    //print_r($data);
    $sql = 'DELETE FROM klsiswa WHERE id = :id ';
    $this->db->query($sql);

    $this->db->bind('id',$data['id']);

    $this->db->execute();

    return $this->db->rowCount();
  }

  public function klSiswa($ta,$kl){
      $sql = "SELECT id,absen,tapel,klsiswa.nis,siswa.nama FROM klsiswa,siswa WHERE tapel = :tapel && kelas = :kelas && siswa.nis = klsiswa.nis ORDER BY nama";

      $this->db->query($sql);

      $this->db->bind( 'tapel' , $ta );
      $this->db->bind( 'kelas' , $kl );

      $siswa = $this->db->resultSet();

      echo json_encode($siswa);
      
  }

  public function siswaKlabs($kls,$abs){
    $sql = "SELECT nis,absen,nama,nmkelas FROM vw_klsiswa WHERE tapel = :tapel && nmkelas = :kelas && absen = :absen";

    $this->db->query($sql);
    $this->db->bind('tapel' , tahunajaran );
    $this->db->bind('kelas' , $kls );
    $this->db->bind('absen' , $abs );

    $data = $this->db->resultOne();
    echo json_encode($data);

  }


}
