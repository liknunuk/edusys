<?php
class Model_users
{
    public function __construct()
    {
        $this->db = new Database();
    }

    public function dataUsers(){
        $sql = "SELECT id,uname,refftable FROM users ORDER BY uname LIMIT ".baris ;
        $this->db->query($sql);
        $user = $this->db->resultSet();
        $i = 0 ;
        $users =[];
        while($i < COUNT($user)){
            if($user[$i]['refftable']=='siswa'){
                $this->db->query("SELECT nama FROM siswa WHERE nis = :nis ");
                $this->db->bind('nis',$user[$i]['uname']);
                $realname = $this->db->resultOne();
            }else{
                $this->db->query("SELECT nama FROM guru WHERE niy = :niy ");
                $this->db->bind('niy',$user[$i]['uname']);
                $realname = $this->db->resultOne();
            }
            $udata = array('uname'=>$user[$i]['uname'],'ugroup'=>$user[$i]['refftable'],'realname'=>$realname['nama']);
            array_push($users,$udata);
            $i++;
        }
        echo json_encode($users);
    }

    public function tambahUser($data){
        $password = md5($data['nisy'] . "_*_" . $data['password'] );
        $sql = " INSERT INTO users VALUES ( NULL , :uname , :ugroup , :upass )";
        $this->db->query($sql);
        $this->db->bind('uname',$data['nisy']);
        $this->db->bind('ugroup',$data['usergroup']);
        $this->db->bind('upass',$password );
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function ngubahUser($data){
        $password = md5($data['nisy'] . "_*_" . $data['password'] );
        $sql = " UPDATE users SET `password` = :upass WHERE uname = :uname";
        $this->db->query($sql);
        $this->db->bind('upass',$password );
        $this->db->bind('uname',$data['nisy']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function hapusUser($data){
        $sql = " DELETE FROM users WHERE refftable = :ugroup && uname = :uname ";
        $this->db->query($sql);
        $this->db->bind('uname',$data['nisy']);
        $this->db->bind('ugroup',$data['table']);
        $this->db->execute();
    }

}