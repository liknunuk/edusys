<?php
class Model_laporan
{
    public function __construct()
    {
        $this->db = new Database();
    }

    public function presensi($tgl,$kls){
        $sql = " SELECT siswa.nama , presensi.hadir, presensi.telat , klsiswa.kelas kdKelas , kelas.kelas FROM siswa , presensi , klsiswa , kelas WHERE klsiswa.nis = presensi.nis && kelas.id = klsiswa.kelas && siswa.nis = presensi.nis && presensi.tanggal = :tgl && klsiswa.kelas = :kls ";
        $this->db->query($sql);
        $this->db->bind('tgl',$tgl);
        $this->db->bind('kls',$kls);
        $this->db->execute();
        $presensi = $this->db->resultSet();
        echo json_encode($presensi);
    }

    public function ibadah($tgl,$kls){
        $kmr =  date( 'Y-m-d', strtotime( $tgl . ' -1 day' ) );

        $sql = "SELECT siswa.nama , sholate.sub , sholate.dhu , sholate.ash , sholate.mag , sholate.ish , klsiswa.kelas kdKelas , kelas.kelas FROM siswa , sholate , klsiswa , kelas WHERE klsiswa.nis = sholate.nis && kelas.id = klsiswa.kelas && siswa.nis = sholate.nis && sholate.tanggal = :tgl && klsiswa.kelas = :kls ORDER BY siswa.nama";

        $this->db->query($sql);
        $this->db->bind('tgl',$kmr);
        $this->db->bind('kls',$kls);
        $this->db->execute();
        $laporan = $this->db->resultSet();
        echo json_encode($laporan);
    }

    public function mood($tgl,$kls){

        $sql = "SELECT siswa.nama , mood.mood , klsiswa.kelas kdKelas , kelas.kelas FROM siswa , mood , klsiswa , kelas WHERE klsiswa.nis = mood.nis && kelas.id = klsiswa.kelas && siswa.nis = mood.nis && mood.tanggal LIKE :tgl && klsiswa.kelas = :kls ORDER BY siswa.nama";

        $this->db->query($sql);
        $this->db->bind('tgl',$tgl."%");
        $this->db->bind('kls',$kls);
        $this->db->execute();
        $laporan = $this->db->resultSet();
        echo json_encode($laporan);
    }

    public function seragam($tgl,$kls){

        $sql = "SELECT siswa.nama , seragame.baju , seragame.rapi , seragame.attr , seragame.sock , seragame.belt , klsiswa.kelas kdKelas , kelas.kelas FROM siswa , seragame , klsiswa , kelas WHERE klsiswa.nis = seragame.nis && kelas.id = klsiswa.kelas && siswa.nis = seragame.nis && seragame.tanggal = :tgl && klsiswa.kelas = :kls ORDER BY siswa.nama";

        $this->db->query($sql);
        $this->db->bind('tgl',$tgl);
        $this->db->bind('kls',$kls);
        $this->db->execute();
        $laporan = $this->db->resultSet();
        echo json_encode($laporan);
    }

}