<?php
class Model_kelas
{
    public function __construct()
    {
        $this->db = new Database();
    }

    public function addKelas($data){
        //print_r($data); 
        $sql = 'INSERT INTO kelas VALUES (:id,:tingkat,:jurusan,:ruang,:kelas)';
        $this->db->query($sql);
        
        $this->db->bind('id',NULL);
        $this->db->bind('tingkat',$data['tingkat']);
        $this->db->bind('jurusan',$data['jurusan']);
        $this->db->bind('ruang',$data['ruang']);
        $this->db->bind('kelas',$data['kelas']);

        $this->db->execute();

        return $this->db->rowCount();
  }

  public function chgKelas($data){
    //print_r($data);
    $sql = 'UPDATE siswa SET tingkat = :tingkat , jurusan = :jurusan , ruang= :ruang , kelas = :kelas WHERE id=:id';
    $this->db->query($sql);

    $this->db->bind('tingkat',$data['tingkat']);
    $this->db->bind('jurusan',$data['jurusan']);
    $this->db->bind('ruang',$data['ruang']);
    $this->db->bind('kelas',$data['kelas']);
    $this->db->bind('id',$data['id']);

    $this->db->execute();

    return $this->db->rowCount();
  }

  public function rmvKelas($data){
    //print_r($data);
    $sql = 'DELETE FROM kelas WHERE id = :id ';
    $this->db->query($sql);

    $this->db->bind('id',$data['id']);

    $this->db->execute();

    return $this->db->rowCount();
  }

  public function dataKelas(){
      $sql = "SELECT * FROM kelas ORDER BY kelas LIMIT " . baris;
      $this->db->query($sql);
      $kelas = $this->db->resultSet();
      echo json_encode($kelas);
  }

  public function caribyname($kelas){
      $sql = "SELECT * FROM kelas WHERE kelas LIKE :kelas";
      $this->db->query($sql);
      $this->db->bind('kelas','%'.$kelas.'%');
      $kelas = $this->db->resultSet();
      echo json_encode($kelas);
  }
}
