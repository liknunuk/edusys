<?php
class Model_siswa
{
    public function __construct()
    {
        $this->db = new Database();
    }

    public function addSiswa($data){
        //print_r($data); 
        $sql = 'INSERT INTO siswa VALUES (:nis,:nama,:hp,:ortu,:alamat, :pkey )';
        $this->db->query($sql);
        
        $this->db->bind('nis',$data['nis']);
        $this->db->bind('nama',$data['nama']);
        $this->db->bind('hp',$data['hp']);
        $this->db->bind('ortu',$data['ortu']);
        $this->db->bind('alamat',$data['alamat']);
        $this->db->bind('pkey',$data['sandi']);
        $this->db->execute();

        return $this->db->rowCount();
  }

  public function chgSiswa($data){
    //print_r($data);
    $sql = 'UPDATE siswa SET nama = :nama, ortu=:ortu, alamat=:alamat , hp = :hp pkey=:pkey WHERE nis = :nis ';
    $this->db->query($sql);

    $this->db->bind('nama',$data['nama']);
    $this->db->bind('ortu',$data['ortu']);
    $this->db->bind('alamat',$data['alamat']);
    $this->db->bind('hp',$data['hp']);
    $this->db->bind('pkey',$data['sandi']);
    $this->db->bind('nis',$data['nis']);

    $this->db->execute();

    return $this->db->rowCount();
  }

  public function rmvSiswa($data){
    //print_r($data);
    $sql = 'DELETE FROM siswa WHERE nis = :nis ';
    $this->db->query($sql);

    $this->db->bind('nis',$data['nis']);

    $this->db->execute();

    return $this->db->rowCount();
  }

  public function dataSiswa(){
      $sql = "SELECT * FROM siswa ORDER BY nis LIMIT " . baris;
      $this->db->query($sql);
      $siswa = $this->db->resultSet();
      echo json_encode($siswa);
  }

  public function caribyname($nama){
      $sql = "SELECT * FROM siswa WHERE nama LIKE :nama ORDER BY nama LIMIT " . baris;
      $this->db->query($sql);
      $this->db->bind('nama','%' . $nama . '%');
      $siswa = $this->db->resultSet();
      echo json_encode($siswa);
  }
}
