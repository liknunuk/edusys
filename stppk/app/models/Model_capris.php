<?php
class Model_capris
{
    public function __construct()
    {
        $this->db = new Database();
    }

    public function saveSholat($data){
        $date_format = 'Y-m-d';
        $today = strtotime('now');
        $yesterday = strtotime('-1 day', $today);

        $qry = $this->db->query( "INSERT INTO sholate VALUES (NULL,:nis,:tapel,:tanggal,:subuh,:dzuhur,:ashar,:maghrib,:isha)");

        $this->db->bind('nis' , $data['nis']);
        $this->db->bind('tapel' , tahunajaran);
        $this->db->bind('tanggal' , gmdate($date_format, $yesterday));
        $this->db->bind('subuh' , $data['subuh']);
        $this->db->bind('dzuhur' , $data['dzuhur']);
        $this->db->bind('ashar' , $data['ashar']);
        $this->db->bind('maghrib' , $data['maghrib']);
        $this->db->bind('isha' , $data['isha']);

        $this->db->execute();
        return $this->db->rowCount();
    }

    public function saveHadir($data){
        $sql = "INSERT INTO presensi (tapel,nis,tanggal,hadir,telat) VALUES (:tapel , :nis , :tanggal , :hadir , :telat) ";
        $this->db->query($sql);
        $this->db->bind('tapel' , tahunajaran );
        $this->db->bind('nis' , $data['nis']);
        $this->db->bind('tanggal' , date('Y-m-d') );
        $this->db->bind('hadir' , $data['hadir'] );
        $this->db->bind('telat' , $data['telat'] );
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function saveSeragam($data){
        $sql = "INSERT INTO seragame (tapel,nis,tanggal,baju,rapi,attr,sock,belt) VALUES ( :tapel , :nis , :tanggal , :baju , :rapi , :attr , :sock , :belt )";
        $this->db->query($sql);
        $this->db->bind('tapel' , tahunajaran );
        $this->db->bind('nis' , $data['nis']);
        $this->db->bind('tanggal' , date('Y-m-d') );
        $this->db->bind('baju' , $data['seragam'] );
        $this->db->bind('rapi' , $data['rapi'] );
        $this->db->bind('attr' , $data['attribut'] );
        $this->db->bind('sock' , $data['kauskaki'] );
        $this->db->bind('belt' , $data['sabuk'] );
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function saveMood($data){
        $sql = "INSERT INTO mood (tapel,nis,mood) VALUES ( :tapel , :nis , :mood )";
        $this->db->query($sql);
        $this->db->bind('tapel' , tahunajaran );
        $this->db->bind('nis' , $data['nis']);
        $this->db->bind('mood' , $data['mood'] );
        $this->db->execute();
        return $this->db->rowCount();
    }
}