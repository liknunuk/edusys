<?php
$dsn = "mysql:host=localhost;dbname=wisatabna";
$options = [
  PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
  PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC,
  PDO::ATTR_EMULATE_PREPARES, false,
  PDO::ATTR_PERSISTENT => true
];

try
{
  $dbh = new PDO($dsn, "root","ma21jr",$options);
}

catch(PDOException $pe)
{
  die('Maaf, gangguan koneksi: ' .$pe->getMessage());
}

$sth = $dbh->prepare("SELECT * FROM eventwisata ");
$sth->execute();

/* Fetch all of the remaining rows in the result set */
print("Fetch all of the remaining rows in the result set:\n");
$result = $sth->fetchAll();
print_r($result);
?>
