$(document).ready(function(){
    $("#lms_openmapel").click(function(){
        $("#datamapel li").remove();
        let kls = $("#tingkat").val(),
            jur = $("#jurusan").val();
        $.getJSON(sources +`lsMapel/${kls}/${jur}` , function(mapels){
            $.each(mapels.data , function(i,data){
                $("#datamapel").append(`<li class='list-group-item' id='${data.kodeMapel}'>${data.namaMapel}<br><small>${data.guru}</small></li>`)
            })
        })
    })

    $("#datamapel").on('click' , '.list-group-item' , function(){
        let kdmp = $(this).prop('id');
        let tnkt = $("#tingkat").val();
        $.getJSON( sources + `cekKontenTkMp/${tnkt}/${kdmp}` , function(konten){
            $("#lmsKonten tr").remove();
            $.each( konten.data , function(i,kntn){
                $("#lmsKonten").append(`
                <tr>
                <td>${kntn.tglPost}</td>
                <td>${kntn.kodeMapel}</td>
                <td>${kntn.namaMapel}</td>
                <td>${kntn.bab}</td>
                <td>${kntn.tingkat}</td>
                <td>
                <a href=javascript:void(0) class='kontendata' id='${kntn.idKonten}'>Lihat</a>
                </td>
                </tr>
                `);
            })
        })
    })
    $("#lmsKonten").on('click' , '.kontendata' , function(){
        let idk = $(this).prop('id');
        window.location=baseurl+`Home/konten/${idk}`;
    })
});