<div class="contaner">
    <div class="row">
        <div class="col-lg-12" id="loginHeader">
            <h4>MEDIA BELAJAR JARAK JAUH</h4>
            <h5>SMK PANCA BHAKTI RAKIT BANJARNEGARA</h5>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">&nbsp;</div>
        <div class="col-lg-4">
          <div id="login-title">L O G I N</div>
          <div id="login-wrapper">
            <form action="<?=BASEURL;?>Home/auth" method="post">
                <div class="form-group">
                    <label for="usname">Nama Pengguna</label>
                    <input type="text" name="usname" id="lms_usname" class="form-control" value="" placeholder="Nomor Induk Siswa">
                </div>

                <div class="form-group">
                    <label for="tasand">Kata Sandi</label>
                    <input type="password" name="tasand" id="lms_tasand" class="form-control" value="">
                </div>

                <div class="form-group">
                    <label for="submit">&nbsp;</label>
                    <input type="submit" class="btn btn-success float-right" value="Login">
                </div>

            </form>
          </div>
          <div id="loginAlert">
          <?php
            if(isset($_SESSION['alert'])){
                echo '
                <div class="alert alert-warning alert-dismissible fade show" role="alert">'.$_SESSION['alert'].'
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                ';
            }
            unset($_SESSION['alert']);
          ?>
          </div>
        </div>
        <div class="col-lg-4">&nbsp;</div>
    </div>
</div>
