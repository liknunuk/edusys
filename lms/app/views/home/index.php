<div class="container-fluid">
  <div class="row">
    <div class="col-sm-4">
    <div class="credensial">
        <h5><?=$_SESSION['nama'];?></h5>
        <h6><?=$_SESSION['nis'];?> | <?=$_SESSION['kelas'];?> | <?=$_SESSION['absen'];?></h6>
    </div>
        <h4>Daftar Mapel</h4>
        <div class="row">
            <div class="col-sm-9">
            <!--form-->
                <div>
                    <div class="input-group input-group-sm mb-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="lms_tingkat">Tingkat</span>
                        </div>
                        <select class="form-control" aria-label="Small" aria-describedby="lms_tingkat" id="tingkat">
                            <option value="X">Kelas 10</option>
                            <option value="XI">Kelas 11</option>
                            <option value="XII">Kelas 12</option>
                        </select>
                    </div>

                    <div class="input-group input-group-sm mb-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="lms_jurusan">Jurusan</span>
                        </div>
                        <select class="form-control" aria-label="Small" aria-describedby="lms_jurusan" id="jurusan">
                            <option value="TKJ">Komputer &amp; Jaringan</option>
                            <option value="TKR">Kendaraan Ringan</option>
                        </select>
                    </div>
                    <!--div class="text-right">
                        <button class="btn btn-primary" id="lms_openmapel">Buka Mapel</button>
                    </div-->
                </div>
            <!--form-->
            </div>
            <div class="col-sm-3">
                <div class="text-right">
                    <button class="btn btn-primary" id="lms_openmapel">Buka Mapel</button>
                </div>
            </div>
        </div>
        <div class="list-group-item" id="datamapel">
        
        </div>
    </div>
    <div class="col-sm-8">
    <?php

    if( $data['konten']['rows'] = 0 ):
    ?>

    <h2 class="text-center">Belum Ada Konten</h2>

    <?php else: ?>
    <div class="card">
        <div class="card-header bg-success text-light">
        <h3>Konten LMS Terkini</h3>
        </div>
        <div class="card-body bg-light">
            <table class="table table-sm table-bordered">
                <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>Kode Mapel</th>
                        <th>Pelajaran</th>
                        <th>Bab</th>
                        <th>Tingkat</th>
                        <th>Kontrol</th>
                    </tr>
                </thead>
                <tbody id="lmsKonten">
                    <!-- tgl,kodeMapel,namaMapel,bab,tingkat -->
                    <?php  foreach($data['konten']['data'] AS $konten): ?>
                    <tr>
                        <td><?=$konten['tglPost'];?></td>
                        <td><?=$konten['kodeMapel'];?></td>
                        <td><?=$konten['namaMapel'];?></td>
                        <td><?=$konten['bab'];?></td>
                        <td><?=$konten['tingkat'];?></td>
                        <td><a href=javascript:void(0) class='kontendata' id='<?=$konten['idKonten'];?>'>Lihat</a></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php endif; ?>
    </div>
  </div>
</div>

<?php $this->view('template/bs4cdn'); ?>
<script src="<?=BASEURL;?>js/index.js"></script>