<?php
$saiki = date('Y-m-d');
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header bg-success text-light">
                    <h4><?=$data['konten']['data']['kodeMapel'];?> - <?=$data['konten']['data']['namaMapel'];?> TINGKAT <?=$data['konten']['data']['tingkat'];?></h4>
                    <p>Diunggah oleh: <?=$data['konten']['data']['nama'];?> - pada: <?=$data['konten']['data']['tglPost'];?></p>
                </div>
                <div class="card-body bg-light">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 list-group">
                                <li class="list-group-item">
                                Bahan Bacaan 
                                <a class='badge badge-primary p-2 float-right' href="<?=BASEURL;?>pfile/<?=$data['konten']['data']['fileKonten'];?>">Unduh</a></li>

                                <li class="list-group-item">
                                Bahan Tugas 
                                <a class='badge badge-primary p-2 float-right' href="<?=BASEURL;?>pfile/<?=$data['konten']['data']['fileTugas'];?>">Unduh</a></li>

                                <li class="list-group-item">
                                Bahan Evaluasi 
                                <a class='badge badge-primary p-2 float-right' href="<?=BASEURL;?>pfile/<?=$data['konten']['data']['fileEvaluasi'];?>">Unduh</a></li>
                                <li class="list-group-item">
                                    <p>Batas Akhir Pengumpulan Tugas : <span class='float-right'><?= ymd2dmy($data['konten']['data']['dlTugas']);?> </span></p>
                                    <p>Batas Akhir Pengumpulan Evaluasi : <span class='float-right'><?=ymd2dmy($data['konten']['data']['dlEvaluasi']);?> </span></p>
                                </li>
                            </div>
                            <div class="col-md-6">
                                <table class="table">
                                    <tbody>
                                        <tbody>
                                            <tr>
                                                <td>Nomor Induk/Nama</td>
                                                <td><?=$_SESSION['nis'];?></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">Unggah Tugas
                                                    <?php if($saiki <= $data['konten']['data']['dlTugas']): ?>
                                                    <form action="<?=BASEURL;?>Home/uptugas" method="post" enctype="multipart/form-data" class="form-inline">
                                                    <input type="hidden" name="nis" value="<?=$_SESSION['nis'];?>">
                                                    <input type="hidden" name="idp" value="<?=$data['konten']['data']['idKonten'];?>">
                                                    <div class="form-group">
                                                        <input type="file" name="ft" id="ft" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-primary">Upload</button>
                                                    </div>
                                                    </form>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">Unggah Evaluasi
                                                <?php if($saiki <= $data['konten']['data']['dlEvaluasi']): ?>
                                                <form action="<?=BASEURL;?>Home/upeval" method="post" enctype="multipart/form-data" class="form-inline">
                                                    <input type="hidden" name="nis" value="<?=$_SESSION['nis'];?>">
                                                    <input type="hidden" name="idp" value="<?=$data['konten']['data']['idKonten'];?>">
                                                    <div class="form-group">
                                                        <input type="file" name="fe" id="fe" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-primary">Upload</button>
                                                    </div>
                                                    </form>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="credensial col-sm-12">
                                <h5><?=$_SESSION['nama'];?></h5>
                                <h6><?=$_SESSION['nis'];?> | <?=$_SESSION['kelas'];?> | <?=$_SESSION['absen'];?></h6>
                            </div>      
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-sm">
                                <thead>
                                    <tr>
                                        <th>No Urut</th>
                                        <th>Nomor Konten</th>
                                        <th>Tanggal jam</th>
                                        <th>Nama Mapel</th>
                                        <th>Nama Berkas</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                    $nomor=1;
                                    foreach($data['mySubs'] AS $subs): 
                                ?>
                                <tr>
                                    <td><?=$nomor;?></td>
                                    <td><?=ucfirst($subs['tipeKoleksi']).' - '.$subs['idKonten'];?></td>
                                    <td><?=$subs['tglKoleksi'];?></td>
                                    <td><?=$subs['namaMapel'];?></td>
                                    <td><?=$subs['fileKoleksi'];?></td>
                                </tr>

                                <?php 
                                    $nomor+=1;
                                endforeach; 
                                ?>
                                </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

function ymd2dmy($tgl){
    $t = explode("-",$tgl);
    return "{$t[2]}/{$t[1]}/{$t[0]}";
}

?>