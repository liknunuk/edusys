<div class="container-fluid">
    <div class="row mt-3">
        <!-- lift side -->
        <div class="col-lg-3">
            <div class="credensial">
                <h5><?=$_SESSION['nama'];?></h5>
                <h6><?=$_SESSION['niy'];?></h6>
                <a href="<?=BASEURL;?>Login/logout">Logout</a>
            </div>
            <!--form-->
            <div>
                <div class="input-group input-group-sm mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="lms_tingkat">Tingkat</span>
                    </div>
                    <select class="form-control" aria-label="Small" aria-describedby="lms_tingkat" id="tingkat">
                        <option value="">Pilih Kelas</option>
                        <option value="X">Kelas 10</option>
                        <option value="XI">Kelas 11</option>
                        <option value="XII">Kelas 12</option>
                    </select>
                </div>

                <div class="input-group input-group-sm mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="lms_jurusan">Jurusan</span>
                    </div>
                    <select class="form-control" aria-label="Small" aria-describedby="lms_jurusan" id="jurusan">
                        <option value="">Pilih Jurusan</option>
                        <option value="TKJ">Komputer &amp; Jaringan</option>
                        <option value="TKR">Kendaraan Ringan</option>
                    </select>
                </div>
                <!--div class="text-right">
                    <button class="btn btn-primary" id="lms_openmapel">Buka Mapel</button>
                </div-->
            </div>
            <!--form-->
            <ul class="list-group mt-3" id="datamapel"></li>
        </div>
        <!-- lift side -->
        <!-- right side -->
        <div class="col-lg-9">
            <table class="table table-sm bg-dark text-light px-2">
                <tbody>
                    <tr>
                        <td>Form Upload Konten</td>
                        <td id="namaMapel"></td>
                        <td id="namaGuru"></td>
                    </tr>
                </tbody>
            </table>
            <!-- upload konten -->
            <form action="<?=BASEURL;?>Guru/upload" method="post" enctype="multipart/form-data">
            <!-- niyGuru,kodeMapel,bab,tingkat,fileKonten,fileTugas,fileEvaluasi,dlTugas,dlEvaluasi -->
            <div class="row bg-secondary py-3">
                <div class="col-md-4">
                    <div class="input-group input-group-sm mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text igt_fuk" id="igk_niy">NIY Guru</span>
                        </div>
                        <input type="text" class="form-control" aria-label="_niy" aria-describedby="igk_niy" name="fuk_niy" id="fuk_niy" readonly>
                    </div>

                    <div class="input-group input-group-sm mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text igt_fuk" id="igk_kdmp">Kode Mapel</span>
                        </div>
                        <input type="text" class="form-control" aria-label="_kdmp" aria-describedby="igk_kdmp" name="fuk_kdmp" id="fuk_kdmp" readonly>
                    </div>

                    <div class="input-group input-group-sm mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text igt_fuk" id="igk_bab">Tingkat</span>
                        </div>
                        <input type="text" class="form-control" aria-label="_tkt" aria-describedby="igk_tkt" name="fuk_tkt" id="fuk_tkt" readonly>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="input-group input-group-sm mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text igt_fuk" id="igk_ref">Berkas Bacaan</span>
                        </div>
                        <input type="file" class="form-control" aria-label="_ref" aria-describedby="igk_ref" name="fuk_ref" id="fuk_ref">
                    </div>

                    <div class="input-group input-group-sm mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text igt_fuk" id="igk_tgs">Berkas Tugas</span>
                        </div>
                        <input type="file" class="form-control" aria-label="_tgs" aria-describedby="igk_tgs" name="fuk_tgs" id="fuk_tgs">
                    </div>

                    <div class="input-group input-group-sm mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text igt_fuk" id="igk_eva">Berkas Evaluasi</span>
                        </div>
                        <input type="file" class="form-control" aria-label="_eva" aria-describedby="igk_eva" name="fuk_eva" id="fuk_eva">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="input-group input-group-sm mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text igt_fuk" id="igk_bab">Bab</span>
                        </div>
                        <input type="number" class="form-control" aria-label="_bab" aria-describedby="igk_bab" name="fuk_bab" id="fuk_bab">
                    </div>

                    <div class="input-group input-group-sm mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text igt_fuk" id="igk_bat">Batas Akhir Tugas</span>
                        </div>
                        <input type="date" class="form-control" aria-label="_bat" aria-describedby="igk_bat" name="fuk_bat" id="fuk_bat">
                    </div>

                    <div class="input-group input-group-sm mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text igt_fuk" id="igk_bae">Batas Akhir Evaluasi</span>
                        </div>
                        <input type="date" class="form-control" aria-label="_bae" aria-describedby="igk_bae" name="fuk_bae" id="fuk_bae">
                    </div>
                    <div class="text-right pr-3">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </div>
            </form>
            <!-- upload konten -->
            <!-- data konten -->
            <div><h3>DATA KONTEN</h3></div>
            <div class="list-group" id="dataKonten"></div>
            <!-- data konten -->
        </div>
        <!-- right side -->
    </div>
</div>

<?php $this->view('template/bs4cdn');?>
<script>const niy ="<?=$_SESSION['niy'];?>";</script>
<script src="<?=BASEURL;?>js/g_index.js"></script>
