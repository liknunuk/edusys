<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h3>Detil Konten</h3>
            <table class="table table-striped">
                <tbody>
                    <tr>
                        <th>ID Konten</th>
                        <td><?=$data['konten']['data']['idKonten'];?></td>
                    </tr>
                    <tr>
                        <th>Diposting pada</th>
                        <td><?=$data['konten']['data']['tglPost'];?>
                        </td></tr>
                    <tr>
                        <th>Diposting oleh</th>
                        <td><?=$data['konten']['data']['nama'];?>&nbsp;
                        [ <?=$data['konten']['data']['niyGuru'];?> ]
                        </td>
                    </tr>
                    <tr>
                        <th>Mata Pelajaran</th>
                        <td>[ <?=$data['konten']['data']['kodeMapel'];?> ]&nbsp;
                        <?=$data['konten']['data']['namaMapel'];?>&nbsp;
                        Bab <?=$data['konten']['data']['bab'];?>&nbsp;
                        Tingkat <?=$data['konten']['data']['tingkat'];?>
                        </td>
                    </tr>
                    </tr>
                    <tr>
                        <th>Berkas Bacaan</th>
                        <td><?=$data['konten']['data']['fileKonten'];?></td>
                    </tr>
                    <tr>
                        <th>Berkas Tugas</th>
                        <td><?=$data['konten']['data']['fileTugas'];?></td>
                    </tr>
                    <tr>
                        <th>Berkas Evaluasi</th>
                        <td><?=$data['konten']['data']['fileEvaluasi'];?></td>
                    </tr>
                </tbody>
            </table>
            <a href="<?=BASEURL?>Guru" class="btn btn-success">Kembali</a>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-lg-12">
            <h3>Berkas Terkumpul</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <h4>Tugas</h4>
            <div class="list-group">
                <?php foreach($data['tugas'] AS $tugas): ?>
                    <li class="list-group-item list-group-item-sm">
                    <?=$tugas['kelas'];?>&nbsp;
                    <?=$tugas['nama'];?>&nbsp;
                    <a href="<?=BASEURL;?>/pfile/<?=$tugas['fileKoleksi'];?>" class="text-danger float-right">Unduh</a>
                    </li>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-lg-6">
            <h4>Evaluasi</h4>
            <div class="list-group">
                <?php foreach($data['evaluasi'] AS $evalu): ?>
                    <li class="list-group-item list-group-item-sm">
                    <?=$evalu['kelas'];?>&nbsp;
                    <?=$evalu['nama'];?>&nbsp;
                    <a href="<?=BASEURL;?>/pfile/<?=$evalu['fileKoleksi'];?>" class="text-danger float-right">Unduh</a>
                    </li>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>