13/9/2019
- view list mapel
CREATE OR REPLACE VIEW vw_ListMapel AS SELECT kontrakMengajar.kodeMapel , mapel.namaMapel , kelas.tingkat , kelas.jurusan FROM kontrakMengajar , mapel , kelas WHERE kelas.id = kontrakMengajar.idKelas && mapel.kodeMapel = kontrakMengajar.kodeMapel GROUP BY mapel.kodeMapel , kelas.tingkat ORDER BY kelas.tingkat , mapel.namaMapel;


- tabel lms_konten
CREATE TABLE lms_konten(
    idKonten int(5) unsiged auto_increment primary key,
    tglPost timestamp default CURRENT_TIMESTAMP(),
    niyGuru varchar(10) not null,
    kodeMapel varchar(20),
    tingkat enum('X','XI','XII') default 'XI',
    fileKonten varchar(15) ,    --tutor00000.docx
    fileTugas varchar(15) ,     --tugas00000.docx
    fileEvaluasi varchar(15),   --evalu00000.docx
    dlTugas date,
    dlEvaluasi date,
    CONSTRAINT fkGuru FOREIGN KEY (niyGuru) REFERENCES guru (niy) ON DELETE CASCADE ON UPDATE RESTRICT,
    CONSTRAINT fkMapel FOREIGN KEY (kodeMapel) REFERENCES mapel (kodeMapel) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB;

18-9-19
-- view kontrak Mapel
CREATE OR REPLACE VIEW vw_kontrakMapel AS SELECT kontrakMengajar.kodeMapel , mapel.namaMapel , kontrakMengajar.idKelas , kelas.tingkat , kelas.jurusan , kontrakMengajar.niyGuru , guru.nama FROM kontrakMengajar , mapel , kelas , guru WHERE mapel.kodeMapel = kontrakMengajar.kodeMapel && kelas.id = kontrakMengajar.idKelas && guru.niy = kontrakMengajar.niyGuru

19-09-19
-- tabel lms_koleksi
CREATE TABLE lms_koleksi(
    idKoleksi int(6) unsigned auto_increment primary key,
    tglKoleksi timestamp default CURRENT_TIMESTAMP,
    nis varchar(10),
    idKonten int(5) unsigned,,
    tipeKoleksi enum('tugas','evaluasi') default 'tugas',
    fileKoleksi tinytext,
    CONSTRAINT fkIdKonten FOREIGN KEY (idKonten) REFERENCES lms_konten(idKonten) ON DELETE CASCADE ON UPDATE RESTRICT
    CONSTRAINT fkNis FOREIGN KEY (nis) REFERENCES siswa(nis) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB;

-- Ganti engine table siswa
ALTER TABLE siswa ENGINE=InnoDB;