<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Home extends Controller
{
  public function __construct(){
    if(!isset($_SESSION['nis'])){
      header("Location:".BASEURL."Login");
    }
  }
  // method default
  public function index()
  {
    $data['title'] = "LMS SMKPB Rakit";
    $data['konten'] = $this->model('Model_konten')->last10Konten();
    $this->view('template/header',$data);
    $this->view('template/navbar');
    
    $this->view('home/index',$data);

    $this->view('template/bs4cdn');
    $this->view('template/footer');
  }

  public function konten($idk){
    $data['title'] = "LMS SMKPB Rakit";
    
    $data['konten']= $this->model('Model_konten')->dataKonten($idk);
    $data['mySubs']= $this->model('Model_siswa')->mySubs($_SESSION['nis']);
    $this->view('template/header',$data);
    $this->view('template/navbar');
    
    $this->view('home/konten',$data);

    $this->view('template/bs4cdn');
    $this->view('template/footer');
  }

  public function login(){
      $data['title'] = "LMS SMKPB Rakit";
      
      
      $this->view('template/header',$data);
      
      $this->view('home/login',$data);
  
      $this->view('template/bs4cdn');
      $this->view('template/footer');
  }

  public function auth(){
    $user = $this->model('Model_users')->login($_POST);
    if( $user['rows'] == 0 ){
      $_SESSION['alert'] = "Username Tidak Ditemukan !!";
      header("Location:".BASEURL."Home/Login");
    }

    $udata = $user['data'];

    if($udata['refftable'] == 'siswa'){
      $data = $this->model('Model_users')->kredentialSiswa($_POST['usname']);
      print_r($data);
      $_SESSION['nis'] = $_POST['usname'];
      $_SESSION['nama'] = $data['nama'];
      $_SESSION['absen'] = $data['absen'];
      $_SESSION['kelas'] = $data['kelas'];

      header("Location:".BASEURL."Home");
    }
    elseif($udata['refftable'] == 'guru'){
      $data = $this->model('Model_users')->kredentialTutor($_POST['usname']);
      $_SESSION['nama'] = $data['nama'];
      $_SESSION['niy'] = $_POST['usname'];
      header("Location:".BASEURL."Guru");
    }
  }

  // upload koleksi tugas/
  public function upTugas(){
        
    $et = $this->getExtension($_FILES['ft']['name']);
    $ftname = 'Tugas_'.$_POST['idp']."-".$_POST['nis'].".".$et;
    $ut = $this->putFile($_FILES['ft'] , $ftname );

    if( $ut == 1 ){
      $data = ['nis'=>$_POST['nis'],'idKonten'=>$_POST['idp'],'tipeKoleksi'=>'tugas','fileKoleksi'=>$ftname];
      if( $this->model('Model_siswa')->uploadKonten($data) > 0){
        header("Location:".BASEURL."Home");
      }
    }
  }

  // upload koleksi evaluasi/
  public function upEval(){
        
    $et = $this->getExtension($_FILES['fe']['name']);
    $ftname = 'Evaluasi_'.$_POST['idp']."-".$_POST['nis'].".".$et;
    $ut = $this->putFile($_FILES['fe'] , $ftname );

    if( $ut == 1 ){
      $data = ['nis'=>$_POST['nis'],'idKonten'=>$_POST['idp'],'tipeKoleksi'=>'evaluasi','fileKoleksi'=>$ftname];
      if( $this->model('Model_siswa')->uploadKonten($data) > 0){
        header("Location:".BASEURL."Home");
      }
    }
  }

  private function getExtension($ufile){
    return (pathinfo($ufile,PATHINFO_EXTENSION));
  }

  private function putFile($ufile,$fname){
    $targetdir= dirname(__FILE__,3)."/public/pfile/";
    $targetFile = $targetdir.$fname;
    if(move_uploaded_file($ufile['tmp_name'],$targetFile)){
      return 1;
    }
  }

}
