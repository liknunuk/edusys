<?php

class Guru extends Controller
{
  public function __construct(){
    if(!isset($_SESSION['niy'])){
      header("Location:".BASEURL."Login");
    }
  }
  // Daftar Mapel yang diampu sebagai halaman induk
  public function index()
  {
    $data['title']  = "LMS Panel Guru";
    $this->view('template/header',$data);
    $this->view('guru/index');
    $this->view('template/footer');
  }

  // upload konten tutorial/
  public function upload(){
    $lastId = $this->model('Model_guru')->lastLmsKontenId();
    
    $ek = $this->getExtension($_FILES['fuk_ref']['name']);
    $et = $this->getExtension($_FILES['fuk_tgs']['name']);
    $ee = $this->getExtension($_FILES['fuk_eva']['name']);

    $kfname = 'Tutor'.$lastId.".".$ek;
    $tfname = 'Tugas'.$lastId.".".$et;
    $efname = 'Evalu'.$lastId.".".$ee;

    $uk = $this->putFile($_FILES['fuk_ref'] , $kfname);
    $ut = $this->putFile($_FILES['fuk_tgs'] , $tfname);
    $ue = $this->putFile($_FILES['fuk_eva'] , $efname);

    if( $uk + $ut + $ue == 3 ){
      $data = ['niy' =>$_POST['fuk_niy'] , 'kdmp'=>$_POST['fuk_kdmp'] , 'bab'=>$_POST['fuk_bab'] , 'tkt'=>$_POST['fuk_tkt'] , 'fk'=>$kfname , 'ft'=>$tfname , 'fe'=>$efname , 'dlt'=>$_POST['fuk_bat'] , 'dle'=>$_POST['fuk_bae']];
      if( $this->model('Model_guru')->uploadKonten($data) > 0){
        header("Location:".BASEURL."Guru");
      }
    }
  }

  public function detil($id){
    $data['konten'] = $this->model('Model_konten')->dataKonten($id);
    $data['tugas'] = $this->model('Model_konten')->getKoleksi($id,'tugas');
    $data['evaluasi'] = $this->model('Model_konten')->getKoleksi($id,'evaluasi');
    $this->view('template/header',$data);
    $this->view('guru/konten',$data);
    $this->view('template/footer');
  }

  private function getExtension($ufile){
    return (pathinfo($ufile,PATHINFO_EXTENSION));
  }

  private function putFile($ufile,$fname){
    $targetdir= dirname(__FILE__,3)."/public/pfile/";
    $targetFile = $targetdir.$fname;
    if(move_uploaded_file($ufile['tmp_name'],$targetFile)){
      return 1;
    }
  }

}
